﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GOST_28147_89
{
  class Program
  {
    static string testKey = "E2C104F9" + "E41D7CDE" + "7FE5E857" + "060265B4" + "281CCC85" + "2E2C929A" + "47464503" + "E00CE510";
    static string testMessage = "123456789";

    static void Main(string[] args)
    {

      int size = 20 * 1024 * 1024;
      byte[] data = new byte[size];
      Random r = new Random(12345);
      r.NextBytes(data);

      //benchmark(data, new GostCounterCrypto());
      //benchmark(data, new GostCounterCryptoMT());
      fileTest(new GostCounterCrypto());
      fileTest(new GostCounterCryptoMT());
      Console.ReadKey();
    }
        
    static void test()
    {
      GostCounterCrypto g = new GostCounterCrypto();
      byte[] key = CryptoUtils.ReadHexArray(testKey);
      int size = 100;
      byte[] message = new byte[size];
      for (int i = 0; i < size; i++) message[i] = (byte)(0x30 + i);

      MemoryStream inputStream = new MemoryStream(message);
      Console.WriteLine(CryptoUtils.WriteHexArray(inputStream.ToArray()));

      MemoryStream encoded = new MemoryStream();
      g.processStream(inputStream, encoded, key);
      string encodedHexMessage = CryptoUtils.WriteHexArray(encoded.ToArray());
      Console.WriteLine(encodedHexMessage);

      MemoryStream decoded = new MemoryStream();
      g.processStream(new MemoryStream(CryptoUtils.ReadHexArray(encodedHexMessage)), decoded, key);
      Console.WriteLine(CryptoUtils.WriteHexArray(decoded.ToArray()));

    }

    static void benchmark(byte[] data,IStreamProcessor crypto)
    {
      byte[] key = CryptoUtils.ReadHexArray(testKey);

      MemoryStream inputFS = new MemoryStream(data);
      MemoryStream outputFS = new MemoryStream();

      Console.WriteLine("Start timer");
      var watch = Stopwatch.StartNew();
      crypto.processStream(inputFS, outputFS, key);
      watch.Stop();
      inputFS.Close();
      outputFS.Close();
      Console.WriteLine("Done in " + watch.ElapsedMilliseconds / 1000.0f + " sec");
    }


    static void fileTest(IStreamProcessor crypto)
    {
      byte[] key = CryptoUtils.ReadHexArray(testKey);

      FileStream inputFS = new FileStream(Console.ReadLine(), FileMode.Open);
      FileStream outputFS = new FileStream(Console.ReadLine(), FileMode.Create);

      Console.WriteLine("Cyphering file");

      Console.WriteLine("Start timer");
      var watch = Stopwatch.StartNew();
      crypto.processStream(inputFS, outputFS, key);
      watch.Stop();
      inputFS.Close();
      outputFS.Close();
      Console.WriteLine("Done in "+watch.ElapsedMilliseconds/1000.0f+" sec");
    }
  }
}
