﻿using System;
using System.Collections.Concurrent;
using System.IO;
using System.Threading.Tasks;

namespace GOST_28147_89
{
  class GostCounterCryptoMT : IStreamProcessor
  {
    public void processStream(Stream input, Stream output, byte[] key)
    {
      UInt32[] keys = getKeySchedule(key);
      int bufferSize = 1024*1024;
      byte[] buffer = new byte[bufferSize];
      UInt64 counter = 0;
      int readCount;

      while ((readCount = input.Read(buffer, 0, bufferSize)) != 0)
      {
        int blocksCount = readCount / 8;
        int bytesNotRead = readCount % 8;
        Parallel.For(0, blocksCount, index =>
        {
          UInt64 cipher = feystelNetCrypt((UInt64)index + counter, keys);
          for (int i = 0; i < 8; i++)
          {
            buffer[index * 8 + i] ^= (byte)(cipher & 0xff);
            cipher >>= 8;
          }
        });
        counter += (UInt64)blocksCount;
        if (bytesNotRead != 0)
        {
          UInt64 cipher = feystelNetCrypt(counter++, keys);
          for (int i = 0; i < bytesNotRead; i++)
          {
            buffer[readCount + i] ^= (byte)(cipher & 0xff);
            cipher >>= 8;
          }
        }

        output.Write(buffer, 0, readCount);
      }
      output.Flush();
    }


    public void processBytes(byte[] input, byte[] output, byte[] key)
    {
    }

    private UInt64 feystelNetCrypt(UInt64 messageBlock, UInt32[] key)
    {
      UInt32 temp;
      UInt32 left = (UInt32)(messageBlock & 0xffffffff);
      UInt32 right = (UInt32)((messageBlock >> 32) & 0xffffffff);
      for (int i = 0; i < 32; i++)
      {


        //UInt32 sum = (UInt32)(((UInt64)left + (UInt64)key[i]) % 0x100000000);
        //int result = 0;

        //result = 
        //  sblocks[0,sum&0xf] | 
        //  sblocks[1,(sum>>4&0xf)]<<4 | 
        //  sblocks[2,(sum>>8&0xf)]<<8 | 
        //  sblocks[3,(sum>>12&0xf)]<<12 | 
        //  sblocks[4,(sum>>16&0xf)]<<16 | 
        //  sblocks[5,(sum>>20&0xf)]<<20 | 
        //  sblocks[6,(sum>>24&0xf)]<<24 | 
        //  sblocks[7,(sum>>28&0xf)]<<28 ;


        //temp = right ^(UInt32) result;
        temp = right ^ f(left, key[i]);
        right = left;
        left = temp;
      }
      return ((UInt64)left << 32) | (UInt64)right;
    }

    private UInt32 f(UInt32 messageHalfBlock, UInt32 key)
    {
      UInt32 sum = (UInt32)(((UInt64)messageHalfBlock + (UInt64)key) % 0x100000000);
      UInt32 result = 0;
      for (int i = 0; i < 8; i++)
      {
        UInt32 a = sum & 0xf;
        a = sblocks[i, a];
        result = result | (a << (4 * i));
        sum >>= 4;
      }

      return (UInt32)((result << 11) | (result >> 21));
    }

    private UInt32[] getKeySchedule(byte[] key256)
    {
      UInt32[] keyArray = new UInt32[8];
      for (int i = 0; i < 32; i += 4)
      {
        keyArray[i / 4] = (UInt32)((key256[i] << 24) | (key256[i + 1] << 16) | (key256[i + 2] << 8) | key256[i + 3]);
      }

      UInt32[] keySequence = new UInt32[32];
      for (int i = 0; i < 24; i++)
      {
        keySequence[i] = keyArray[i % 8];
      }
      for (int i = 0; i < 8; i++)
      {
        keySequence[31 - i] = keyArray[i];
      }
      return keySequence;
    }

    byte[,] sblocks = new byte[,]{
                {0x4,0xA,0x9,0x2,0xD,0x8,0x0,0xE,0x6,0xB,0x1,0xC,0x7,0xF,0x5,0x3},
                {0xE,0xB,0x4,0xC,0x6,0xD,0xF,0xA,0x2,0x3,0x8,0x1,0x0,0x7,0x5,0x9},
                {0x5,0x8,0x1,0xD,0xA,0x3,0x4,0x2,0xE,0xF,0xC,0x7,0x6,0x0,0x9,0xB},
                {0x7,0xD,0xA,0x1,0x0,0x8,0x9,0xF,0xE,0x4,0x6,0xC,0xB,0x2,0x5,0x3},
                {0x6,0xC,0x7,0x1,0x5,0xF,0xD,0x8,0x4,0xA,0x9,0xE,0x0,0x3,0xB,0x2},
                {0x4,0xB,0xA,0x0,0x7,0x2,0x1,0xD,0x3,0x6,0x8,0x5,0x9,0xC,0xF,0xE},
                {0xD,0xB,0x4,0x1,0x3,0xF,0x5,0x9,0x0,0xA,0xE,0x7,0x6,0x8,0x2,0xC},
                {0x1,0xF,0xD,0x0,0x5,0x7,0xA,0x4,0x9,0x2,0x3,0xE,0x6,0xB,0x8,0xC}
            };


  }
}