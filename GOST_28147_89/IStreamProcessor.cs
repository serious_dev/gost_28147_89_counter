﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GOST_28147_89
{
  interface IStreamProcessor
  {
    void processStream(Stream input, Stream output, byte[] key);
  }
}
